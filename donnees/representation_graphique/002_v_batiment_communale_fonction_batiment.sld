<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:se="http://www.opengis.net/se" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" version="1.1.0" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <se:Name>v_batiment_com</se:Name>
    <UserStyle>
      <se:Name>v_batiment_com</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>Commerce</se:Name>
          <se:Description>
            <se:Title>Commerce</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>fonction_batiment</ogc:PropertyName>
              <ogc:Literal>01</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		  <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#267F00</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>50</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/fonction_batiment/commerce.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>60</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>Habitation</se:Name>
          <se:Description>
            <se:Title>Habitation</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>fonction_batiment</ogc:PropertyName>
              <ogc:Literal>02</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		  <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#7777FF</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>50</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
          <se:PointSymbolizer>
			<se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/fonction_batiment/habitation.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>60</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>Association</se:Name>
          <se:Description>
            <se:Title>Association</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>fonction_batiment</ogc:PropertyName>
              <ogc:Literal>03</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		   <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#2F5FB7</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>50</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
          <se:PointSymbolizer>
			<se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/fonction_batiment/association.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>60</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>Admnistratif</se:Name>
          <se:Description>
            <se:Title>Admnistratif</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>fonction_batiment</ogc:PropertyName>
              <ogc:Literal>04</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		  <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#7108B2</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>50</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
          <se:PointSymbolizer>
			<se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/fonction_batiment/administratif.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>60</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>Technique</se:Name>
          <se:Description>
            <se:Title>Technique</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>fonction_batiment</ogc:PropertyName>
              <ogc:Literal>05</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		  	<se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#C16D00</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>50</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
          <se:PointSymbolizer>
			<se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/fonction_batiment/technique.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>60</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>Autre</se:Name>
          <se:Description>
            <se:Title>Autre</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>fonction_batiment</ogc:PropertyName>
              <ogc:Literal>06</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
		  <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#444444</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>50</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
          <se:PointSymbolizer>
			<se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/fonction_batiment/autre.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>60</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>Sans fonction</se:Name>
          <se:Description>
            <se:Title>Sans fonction</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsNull>
              <ogc:PropertyName>fonction_batiment</ogc:PropertyName>
            </ogc:PropertyIsNull>
          </ogc:Filter>
		  <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#CECECE</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>50</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
          <se:PointSymbolizer>
			<se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/fonction_batiment/sans_fonction.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>60</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
