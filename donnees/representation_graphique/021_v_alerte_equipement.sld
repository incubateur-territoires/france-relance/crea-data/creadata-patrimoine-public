<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" version="1.1.0" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:se="http://www.opengis.net/se">
  <NamedLayer>
    <se:Name>v_semiologie_prochain_controle</se:Name>
    <UserStyle>
      <se:Name>v_semiologie_prochain_controle</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>equipement_3_mois</se:Name>
          <se:Description>
            <se:Title>Avant 3 mois</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>equipement_3_mois</ogc:PropertyName>
              <ogc:Literal>true</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>pentagon</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#ffffff</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#FAF306</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">5</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>80</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
		  <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/controle/equipement_3mois.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>45</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>equipement_1_mois</se:Name>
          <se:Description>
            <se:Title>Avant 1 mois</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>equipement_1_mois</ogc:PropertyName>
              <ogc:Literal>true</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>pentagon</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#ffffff</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#F98B08</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">5</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>80</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
		  <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/controle/equipement_1mois.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>45</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>equipement_depasse</se:Name>
          <se:Description>
            <se:Title>Date dépassée</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>equipement_depasse</ogc:PropertyName>
              <ogc:Literal>true</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>pentagon</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#ffffff</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#F90808</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">5</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>80</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
		  <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/controle/equipement_depasse.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>45</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
