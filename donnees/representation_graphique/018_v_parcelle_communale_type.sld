<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" version="1.1.0" xmlns:ogc="http://www.opengis.net/ogc" xmlns:se="http://www.opengis.net/se" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <se:Name>v_geo_parcelle_commune</se:Name>
    <UserStyle>
      <se:Name>v_geo_parcelle_commune</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>Voirie</se:Name>
          <se:Description>
            <se:Title>Voirie</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>type_parcelle</ogc:PropertyName>
              <ogc:Literal>1</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/parcelle/voirie.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>80</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>Agricole</se:Name>
          <se:Description>
            <se:Title>Agricole</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>type_parcelle</ogc:PropertyName>
              <ogc:Literal>2</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/parcelle/agricole.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>80</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>En friche</se:Name>
          <se:Description>
            <se:Title>En friche</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>type_parcelle</ogc:PropertyName>
              <ogc:Literal>3</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/parcelle/friche.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>80</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>Friche industrielle</se:Name>
          <se:Description>
            <se:Title>Friche industrielle</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>type_parcelle</ogc:PropertyName>
              <ogc:Literal>4</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/parcelle/friche_industrielle.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>80</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>Bois</se:Name>
          <se:Description>
            <se:Title>Bois</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>type_parcelle</ogc:PropertyName>
              <ogc:Literal>5</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/parcelle/bois1.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>80</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>Autre</se:Name>
          <se:Description>
            <se:Title>Autre</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>type_parcelle</ogc:PropertyName>
              <ogc:Literal>6</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
 		 <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#CE74F0</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>40</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
          <se:Name>Inconnu</se:Name>
          <se:Description>
            <se:Title>Inconnu</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>type_parcelle</ogc:PropertyName>
              <ogc:Literal>7</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
 		 <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#D4DFD3</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>40</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
          <se:Name>Locatif (fermage agricole)</se:Name>
          <se:Description>
            <se:Title>Locatif (fermage agricole)</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>type_parcelle</ogc:PropertyName>
              <ogc:Literal>8</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
 		 <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
				<se:Fill>
                  <se:SvgParameter name="fill">#46B43D</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#000000</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">1</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>40</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
		            <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/parcelle/agricole.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>80</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
                <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/euros.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>25</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
