<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:se="http://www.opengis.net/se" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" version="1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc">
  <NamedLayer>
    <se:Name>geo_bati_commune</se:Name>
    <UserStyle>
      <se:Name>geo_bati_commune</se:Name>
      <se:FeatureTypeStyle>
        <se:Rule>
          <se:Name>A</se:Name>
          <se:Description>
            <se:Title>A</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>classe_energetique</ogc:PropertyName>
              <ogc:Literal>02</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/classe_energetique/Classe_A.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                    <ogc:Literal>60</ogc:Literal>
                  </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
           <se:Name>B</se:Name>
          <se:Description>
            <se:Title>B</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>classe_energetique</ogc:PropertyName>
              <ogc:Literal>03</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/classe_energetique/Classe_B.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>80</ogc:Literal>
              </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
        <se:Rule>
           <se:Name>C</se:Name>
          <se:Description>
            <se:Title>C</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>classe_energetique</ogc:PropertyName>
              <ogc:Literal>04</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/classe_energetique/Classe_C.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>80</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
	    <se:Rule>
            <se:Name>D</se:Name>
          <se:Description>
            <se:Title>D</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>classe_energetique</ogc:PropertyName>
              <ogc:Literal>05</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/classe_energetique/Classe_D.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>80</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
            <se:Name>E</se:Name>
          <se:Description>
            <se:Title>E</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>classe_energetique</ogc:PropertyName>
              <ogc:Literal>06</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/classe_energetique/Classe_E.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>80</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
			<se:Name>F</se:Name>
          <se:Description>
            <se:Title>F</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>classe_energetique</ogc:PropertyName>
              <ogc:Literal>07</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/classe_energetique/Classe_F.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>80</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
			 <se:Name>G</se:Name>
          <se:Description>
            <se:Title>G</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>classe_energetique</ogc:PropertyName>
              <ogc:Literal>08</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:ExternalGraphic>
               <se:OnlineResource xlink:type="simple" xlink:href="https://atd16.sirap.fr/xmap/files/sw_symbols/atd16/gestion_territoriale/classe_energetique/Classe_G.png" />
				<se:Format>image/png</se:Format>
              </se:ExternalGraphic>
              <se:Size>
                <ogc:Literal>80</ogc:Literal>
               </se:Size>			  
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
			  <se:Name>Classe inconnu</se:Name>
          <se:Description>
            <se:Title>Classe inconnu</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsEqualTo>
              <ogc:PropertyName>classe_energetique</ogc:PropertyName>
              <ogc:Literal>09</ogc:Literal>
            </ogc:PropertyIsEqualTo>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#9808080</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#232323</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">0.5</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>30</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
		<se:Rule>
			  <se:Name>Pas de classe énergetique</se:Name>
          <se:Description>
            <se:Title>Pas de classe énergetique</se:Title>
          </se:Description>
          <ogc:Filter xmlns:ogc="http://www.opengis.net/ogc">
            <ogc:PropertyIsNull>
              <ogc:PropertyName>classe_energetique</ogc:PropertyName>
            </ogc:PropertyIsNull>
          </ogc:Filter>
          <se:PointSymbolizer>
            <se:Graphic>
              <se:Mark>
                <se:WellKnownName>circle</se:WellKnownName>
                <se:Fill>
                  <se:SvgParameter name="fill">#FFFFFF</se:SvgParameter>
                </se:Fill>
                <se:Stroke>
                  <se:SvgParameter name="stroke">#232323</se:SvgParameter>
                  <se:SvgParameter name="stroke-width">0.5</se:SvgParameter>
                </se:Stroke>
              </se:Mark>
              <se:Size>30</se:Size>
            </se:Graphic>
          </se:PointSymbolizer>
        </se:Rule>
      </se:FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
