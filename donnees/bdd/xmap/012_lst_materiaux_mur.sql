-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur des materiaux pour les murs                                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################



CREATE TABLE atd16_patrimoine_foncier.lst_materiaux_mur
(
    gid serial NOT NULL,
    code character varying(10),
    libelle character varying(256),
	"order" integer ,
    CONSTRAINT pk_lst_materiaux_mur PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
;
ALTER TABLE atd16_patrimoine_foncier.lst_materiaux_mur
    OWNER to sditecgrp;

GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_materiaux_mur TO sditecgrp;

/* Liste de valeur des matiere de la toiture*/

INSERT INTO atd16_patrimoine_foncier.lst_materiaux_mur (code, libelle, "order")
	VALUES
('00','Autre','99'),
('01','Bloc béton (Parpaing)','1'),
('02','Béton cellulaire','2'),
('03','Brique','3'),
('04', 'Métal','4'),
('05','Tôle','5'),
('06','Zinc','6'),
('07','Materiaux écologique','7'),
('08','Moellon','8'),
('09','Pierre','9');
