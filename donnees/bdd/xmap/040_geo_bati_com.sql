-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git
-- 2022/04/05 : DC / Ajout du champ insee dans les vues
-- 2022/04/06 : DC / Modification du geom dans la vue v_batiment_com
-- 2022/04/08 : DC / création du trigger de la clé IGN
-- 2022/04/08 : DC / Suppression de la vue des travaux
-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table géographique : des batiments des communes                                                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


CREATE TABLE atd16_patrimoine_foncier.geo_bati_com
(
    gid serial NOT NULL,--[PK][ATD16] Identifiant unique généré automatiquement
    cleabs character varying(200),--[IGN] Identifiant unique de l'IGN(Automatique)
    insee character varying(6),--[ATD16] Code insee de la commune
	numero_batiment character varying (100),--[ATD16] Numéro du bâtiment
    ident character varying(45),--[ATD16] description du bâtiment
	batiment_erp boolean,--[ATD16] bâtiment erp oui/non case à cocher
	type_batiment character varying(2),--[ATD16] Liste déroulante lst_type_batiment
	sommeil boolean,--[ATD16] bâtiment avec sommeil oui/non case à cocher
	utilisation_batiment character varying (254),--[ATD16] Utlisation du bâtiment
	fonction_batiment character varying (2),--[ATD16] Liste déroulante lst_fonction_batiment
    location boolean,--[ATD16] bâtiment avec location oui/non case à cocher
    surface character varying(50),--[ATD16] Surface du bâtiment
	hauteur character varying(50),--[ATD16] Hauteur du bâtiment
    materiaux_toiture character varying(2),--[ATD16] Liste déroulante lst_materiaux_toiture
	materiaux_mur character varying(2),--[ATD16] Liste déroulante lst_materiaux_mur
	autre_materiaux_mur character varying (254),--[ATD16] materiaux qui ne sont pas dans la liste
    observation character varying(255),--[ATD16] Diverses observations
	classe_energetique character varying(2),--[ATD16] Liste déroulante 
	categorie character varying (2),--[ATD16] Liste déroulante 
	nom_prop character varying (254),--[ATD16] Nom du propriétaire (Automatique)
    date_creation timestamp,--[ATD16]Date de création de la donnée 
    date_maj timestamp,--[ATD16] Date de mise à jour de la donnée
    origdata character varying (254),--[ATD16] Origine de la donnée
    the_geom geometry,--[ATD16] Géometry du point
    CONSTRAINT pk_geo_bati_com PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE atd16_patrimoine_foncier.geo_bati_com
    OWNER to sditecgrp;
-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###   v_batiment_com : vue permettant une visibilité  des bâtiments des communes avec les données  de l'ign                                    ###
-- ##################################################################################################################################################


CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_batiment_com
 AS
 SELECT 
	a.cleabs,
    a.nature,
    a.usage_1,
    a.usage_2,
    a.construction_legere,
    a.etat_de_l_objet,
    a.gcms_date_creation,
    a.gcms_date_modification,
    a.date_d_apparition,
    a.date_de_confirmation,
    a.sources,
    a.identifiants_sources,
    a.precision_planimetrique,
    a.precision_altimetrique,
    a.nombre_de_logements,
    a.nombre_d_etages,
    a.materiaux_des_murs,
    a.materiaux_de_la_toiture,
    a.hauteur,
    a.altitude_minimale_sol ,
    a.altitude_minimale_toit,
    a.altitude_maximale_toit,
    a.altitude_maximale_sol,
    a.origine_du_batiment,
    a.appariement_fichiers_fonciers,
	a.millesime,
	a.origdata,
	b.gid,
    b.cleabs as id_bdtopo,
    b.insee,
	b.numero_batiment,
    b.ident,
	b.batiment_erp,
	b.type_batiment,
	b.utilisation_batiment,
	b.fonction_batiment,
    b.location,
    b.surface,
	b.hauteur as hauteur_commune,
	b.categorie,
	b.sommeil,
    b.materiaux_toiture,
	b.materiaux_mur,
	b.autre_materiaux_mur,
    b.observation,
	b.classe_energetique,
	b.nom_prop,
	b.the_geom
   FROM atd16_patrimoine_foncier.geo_bati_com as b  
  LEFT JOIN ign_bdtopo.geo_batiment as a  ON a.cleabs = b.cleabs;
  ALTER TABLE atd16_patrimoine_foncier.v_batiment_com
    OWNER TO sditecgrp;
	
--    ##################################################################################################################################################
-- ###   v_date_dernier_controle_com : vue permettant de calculer la date du dernier contrôle de chaque bâtiment                                  ###
-- ##################################################################################################################################################
CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_date_dernier_controle_com
 AS
Select


a.gid,
a.ident,
b.id_gid,
b.objet_du_controle,
MAX(b.date_du_controle) AS dernier_controle
			
FROM atd16_patrimoine_foncier.ngeo_controle_bati_com b

LEFT JOIN atd16_patrimoine_foncier.geo_bati_com as a  ON a.gid::text = b.id_gid::text 

GROUP BY (a.the_geom, b.id_gid, a.gid,a.ident, b.objet_du_controle);

-- ##################################################################################################################################################
-- ###   v_date_prochain_controle_com : vue permettant de faire remonter la date du prochain controle de chaque bâtiment                          ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_date_prochain_controle_com
AS
Select


a.gid,
a.numero_batiment,
b.id_gid,
b.objet_du_controle,
MAX(b.date_du_prochain_controle) AS prochain_controle
			
FROM atd16_patrimoine_foncier.ngeo_controle_bati_com b

LEFT JOIN atd16_patrimoine_foncier.geo_bati_com as a  ON a.gid::text = b.id_gid::text 

GROUP BY (a.the_geom, b.id_gid, a.gid,a.numero_batiment, b.objet_du_controle);

-- ##################################################################################################################################################
-- ### 
 ---------------------------------------------------------Vue qui sert pour creer les alertes des controles---------------------------------------------------------
-- ##################################################################################################################################################

 CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_prochain_controle_incendie_com
 AS
Select

a.gid,
b.insee,
b.the_geom,
b.numero_batiment,
d.dernier_controle,
a.prochain_controle,
a.objet_du_controle,
(((a.prochain_controle)- interval '3' MONTH) <= current_date) AS incendie_3_mois,
(((a.prochain_controle) - interval '1' MONTH) <= current_date)	AS incendie_1_mois,
(((a.prochain_controle) <= current_date)	OR  (c.date_du_prochain_controle IS NULL)) AS incendie_depasse

FROM atd16_patrimoine_foncier.v_date_prochain_controle_com AS a
LEFT JOIN atd16_patrimoine_foncier.geo_bati_com AS b ON b.gid = a.gid
LEFT JOIN atd16_patrimoine_foncier.ngeo_controle_bati_com AS c ON c.id_gid = b.gid
LEFT JOIN atd16_patrimoine_foncier.v_date_dernier_controle_com AS d ON d.gid = b.gid

Where a.objet_du_controle = '1' AND d.objet_du_controle = '1'

GROUP BY (a.gid, b.insee, b.the_geom, b.numero_batiment, a.objet_du_controle,
a.prochain_controle, d.dernier_controle,
incendie_3_mois, incendie_1_mois,incendie_depasse);

-- ##################################################################################################################################################
-- ###   Vue des alertes controles des detecteurs de fumée des communes                      ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_prochain_controle_fumee_com
 AS
Select

a.gid,
b.insee,
b.the_geom,
b.numero_batiment,
d.dernier_controle,
a.prochain_controle,
a.objet_du_controle,
(((a.prochain_controle) - interval '3' MONTH) <= current_date) AS fumee_3_mois,
(((a.prochain_controle) - interval '1' MONTH) <= current_date) AS fumee_1_mois,
(((a.prochain_controle) <= current_date) OR  (c.date_du_prochain_controle IS NULL)) AS fumee_depasse

FROM atd16_patrimoine_foncier.v_date_prochain_controle_com AS a
LEFT JOIN atd16_patrimoine_foncier.geo_bati_com AS b ON b.gid = a.gid
LEFT JOIN atd16_patrimoine_foncier.ngeo_controle_bati_com AS c ON c.id_gid = b.gid
LEFT JOIN atd16_patrimoine_foncier.v_date_dernier_controle_com AS d ON d.gid = b.gid

Where a.objet_du_controle = '2' AND d.objet_du_controle = '2'


GROUP BY (a.gid, b.insee, b.the_geom, b.numero_batiment, d.dernier_controle, a.prochain_controle, a.objet_du_controle,
fumee_3_mois, fumee_1_mois, fumee_depasse);

-- ##################################################################################################################################################
-- ###   Vue des alertes controles des detecteurs de electricitée des communes                  ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_prochain_controle_electricite_com
 AS
Select

a.gid,
b.insee,
b.the_geom,
b.numero_batiment,
d.dernier_controle,
a.prochain_controle,
a.objet_du_controle,
(((a.prochain_controle) - interval '3' MONTH) <= current_date) AS electricite_3_mois,
(((a.prochain_controle) - interval '1' MONTH) <= current_date) AS electricite_1_mois,
(((a.prochain_controle) <= current_date) OR  (c.date_du_prochain_controle IS NULL)) AS electricite_depasse

FROM atd16_patrimoine_foncier.v_date_prochain_controle_com AS a
LEFT JOIN atd16_patrimoine_foncier.geo_bati_com AS b ON b.gid = a.gid
LEFT JOIN atd16_patrimoine_foncier.ngeo_controle_bati_com AS c ON c.id_gid = b.gid
LEFT JOIN atd16_patrimoine_foncier.v_date_dernier_controle_com AS d ON d.gid = b.gid

Where a.objet_du_controle = '3' AND d.objet_du_controle = '3'

GROUP BY (a.gid, b.insee, b.the_geom,b.numero_batiment, d.dernier_controle, a.prochain_controle,a.objet_du_controle,
electricite_3_mois, electricite_1_mois, electricite_depasse);

-- ##################################################################################################################################################
-- ###   Vue des alertes controles des gaz des communes                    ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_prochain_controle_gaz_com
 AS
Select

a.gid,
b.insee,
b.the_geom,
b.numero_batiment,
d.dernier_controle,
a.prochain_controle,
a.objet_du_controle,
(((a.prochain_controle) - interval '3' MONTH) <= current_date) AS gaz_3_mois,
(((a.prochain_controle) - interval '1' MONTH) <= current_date) AS gaz_1_mois,
(((a.prochain_controle) <= current_date) OR  (c.date_du_prochain_controle IS NULL)) AS gaz_depasse

FROM atd16_patrimoine_foncier.v_date_prochain_controle_com AS a
LEFT JOIN atd16_patrimoine_foncier.geo_bati_com AS b ON b.gid = a.gid
LEFT JOIN atd16_patrimoine_foncier.ngeo_controle_bati_com AS c ON c.id_gid = b.gid
LEFT JOIN atd16_patrimoine_foncier.v_date_dernier_controle_com AS d ON d.gid = b.gid

Where a.objet_du_controle = '4' AND d.objet_du_controle = '4'

GROUP BY (a.gid, b.insee, b.the_geom,b.numero_batiment, d.dernier_controle, a.prochain_controle,a.objet_du_controle,
gaz_3_mois, gaz_1_mois, gaz_depasse);

-- ##################################################################################################################################################
-- ###   Vue des alertes controles du chauffage des communes                     ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_prochain_controle_chauffage_com
 AS
Select

a.gid,
b.insee,
b.the_geom,
b.numero_batiment,
d.dernier_controle,
a.prochain_controle,
a.objet_du_controle,
(((a.prochain_controle) - interval '3' MONTH) <= current_date) AS chauffage_3_mois,
(((a.prochain_controle) - interval '1' MONTH) <= current_date) AS chauffage_1_mois,
(((a.prochain_controle) <= current_date) OR  (c.date_du_prochain_controle IS NULL))	AS chauffage_depasse

FROM atd16_patrimoine_foncier.v_date_prochain_controle_com AS a
LEFT JOIN atd16_patrimoine_foncier.geo_bati_com AS b ON b.gid = a.gid
LEFT JOIN atd16_patrimoine_foncier.ngeo_controle_bati_com AS c ON c.id_gid = b.gid
LEFT JOIN atd16_patrimoine_foncier.v_date_dernier_controle_com AS d ON d.gid = b.gid

Where a.objet_du_controle = '5' AND d.objet_du_controle = '5'

GROUP BY (a.gid, b.insee, b.the_geom,b.numero_batiment, d.dernier_controle, a.prochain_controle,a.objet_du_controle,
chauffage_3_mois, chauffage_1_mois, chauffage_depasse);

-- ##################################################################################################################################################
-- ###   Vue des alertes controles de desenfumage des communes                    ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_prochain_controle_desenfumage_com
 AS
Select

a.gid,
b.insee,
b.the_geom,
b.numero_batiment,
d.dernier_controle,
a.prochain_controle,
a.objet_du_controle,
(((a.prochain_controle) - interval '3' MONTH) <= current_date) AS desenfumage_3_mois,
(((a.prochain_controle) - interval '1' MONTH) <= current_date) AS desenfumage_1_mois,
(((a.prochain_controle) <= current_date) OR  (c.date_du_prochain_controle IS NULL)) AS desenfumage_depasse

FROM atd16_patrimoine_foncier.v_date_prochain_controle_com AS a
LEFT JOIN atd16_patrimoine_foncier.geo_bati_com AS b ON b.gid = a.gid
LEFT JOIN atd16_patrimoine_foncier.ngeo_controle_bati_com AS c ON c.id_gid = b.gid
LEFT JOIN atd16_patrimoine_foncier.v_date_dernier_controle_com AS d ON d.gid = b.gid

Where a.objet_du_controle = '6' AND d.objet_du_controle = '6'

GROUP BY (a.gid, b.insee, b.the_geom,b.numero_batiment, d.dernier_controle, a.prochain_controle,a.objet_du_controle,
desenfumage_3_mois, desenfumage_1_mois, desenfumage_depasse);

-- ##################################################################################################################################################
-- ###    Vue des prochains contrôles des cuisines des communes                      ###
-- ##################################################################################################################################################


CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_prochain_controle_cuisine_com
 AS
Select

a.gid,
b.insee,
b.the_geom,
b.numero_batiment,
d.dernier_controle,
a.prochain_controle,
a.objet_du_controle,
(((a.prochain_controle) - interval '3' MONTH) <= current_date) AS cuisine_3_mois,
(((a.prochain_controle) - interval '1' MONTH) <= current_date) AS cuisine_1_mois,
(((a.prochain_controle) <= current_date) OR  (c.date_du_prochain_controle IS NULL)) AS cuisine_depasse

FROM atd16_patrimoine_foncier.v_date_prochain_controle_com AS a
LEFT JOIN atd16_patrimoine_foncier.geo_bati_com AS b ON b.gid = a.gid
LEFT JOIN atd16_patrimoine_foncier.ngeo_controle_bati_com AS c ON c.id_gid = b.gid
LEFT JOIN atd16_patrimoine_foncier.v_date_dernier_controle_com AS d ON d.gid = b.gid

Where a.objet_du_controle = '7' AND d.objet_du_controle = '7'

GROUP BY (a.gid, b.insee, b.the_geom,b.numero_batiment, d.dernier_controle, a.prochain_controle,a.objet_du_controle,
cuisine_3_mois, cuisine_1_mois, cuisine_depasse);

-- ##################################################################################################################################################
-- ###    Vue des alertes controles des ascenseurs des communes                      ###
-- ##################################################################################################################################################


CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_prochain_controle_ascenseur_com
 AS
Select

a.gid,
b.insee,
b.the_geom,
b.numero_batiment,
d.dernier_controle,
a.prochain_controle,
a.objet_du_controle,
(((a.prochain_controle) - interval '3' MONTH) <= current_date) AS ascenseur_3_mois,
(((a.prochain_controle) - interval '1' MONTH) <= current_date) AS ascenseur_1_mois,
(((a.prochain_controle) <= current_date) OR  (c.date_du_prochain_controle IS NULL)) AS ascenseur_depasse

FROM atd16_patrimoine_foncier.v_date_prochain_controle_com AS a
LEFT JOIN atd16_patrimoine_foncier.geo_bati_com AS b ON b.gid = a.gid
LEFT JOIN atd16_patrimoine_foncier.ngeo_controle_bati_com AS c ON c.id_gid = b.gid
LEFT JOIN atd16_patrimoine_foncier.v_date_dernier_controle_com AS d ON d.gid = b.gid

Where a.objet_du_controle = '8' AND d.objet_du_controle = '8'

GROUP BY (a.gid, b.insee, b.the_geom,b.numero_batiment, d.dernier_controle, a.prochain_controle,a.objet_du_controle,
ascenseur_3_mois, ascenseur_1_mois, ascenseur_depasse);

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################


CREATE TRIGGER t_before_i_date_creation_bati_com
BEFORE INSERT
ON atd16_patrimoine_foncier.geo_bati_com
FOR EACH ROW
EXECUTE PROCEDURE atd16_patrimoine_foncier.f_datecreation();

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_u_date_maj_geo_bati_com
    BEFORE UPDATE 
    ON atd16_patrimoine_foncier.geo_bati_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_patrimoine_foncier.f_date_maj();


-- ##################################################################################################################################################
-- ###                                                      Insertion de la clé IGN                                                                 ###
-- ##################################################################################################################################################
CREATE TRIGGER t_b_iu_cleabs_com
BEFORE INSERT OR UPDATE 
ON atd16_patrimoine_foncier.geo_bati_com
FOR EACH ROW
EXECUTE FUNCTION atd16_patrimoine_foncier.f_cleabs_ign();
