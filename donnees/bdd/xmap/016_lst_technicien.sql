-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur des technicien de contrôle                                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


CREATE TABLE atd16_patrimoine_foncier.lst_technicien
(
    gid serial NOT NULL,
    code integer,
    libelle character varying(256),
	"order" integer, 
    CONSTRAINT pk_lst_technicien PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
;
ALTER TABLE atd16_patrimoine_foncier.lst_technicien
    OWNER to sditecgrp;

GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_technicien TO sditecgrp;

/* Liste de valeur des techniciens*/

INSERT INTO atd16_patrimoine_foncier.lst_technicien  (code, libelle,"order")
	VALUES
('00','','00'),	
('01','Technicien compétent avec habilitation','01'),
('02','Contrat d''entretien','02'),
('03','Organisme agréé','03');
