-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/04/21 : DC / Création de la table des contrôles des équipements
-- 2022/04/21 : DC / Créationdes triggers génériques date création et date maj


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                   Table non géographique : du contrôle des équipements des cdc                                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


CREATE TABLE atd16_patrimoine_foncier.ngeo_controle_equipement_cdc
(
    gid serial NOT NULL,--[PK][ATD16] Identifiant unique généré automatiquement
	id_gid integer,--[ATD16] Identifiant de l'équipement
    ident character varying(255) ,--[ATD16] Référence du contrôle
    date_du_controle date,--[ATD16] Date du contrôle
    objet_du_controle character varying(255),--[ATD16]Objet du contrôle
    controleur character varying(255),--[ATD16]Qui à réalisé le contrôle
    date_du_prochain_controle date,--[ATD16] Date du prochain contrôle
    date_creation timestamp without time zone,--[ATD16]Date de création de la donnée 
    date_maj timestamp without time zone,--[ATD16] Date de mise à jour de la donnée
    CONSTRAINT pk_ngeo_controle_equipement_cdc PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE atd16_patrimoine_foncier.ngeo_controle_equipement_cdc
    OWNER to sditecgrp;


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_i_date_creation_controle_equipement_cdc
BEFORE INSERT
ON atd16_patrimoine_foncier.ngeo_controle_equipement_cdc
FOR EACH ROW
EXECUTE PROCEDURE atd16_patrimoine_foncier.f_datecreation();


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_u_date_maj_ngeo_controle_equipement_cdc
    BEFORE UPDATE 
    ON atd16_patrimoine_foncier.ngeo_controle_equipement_cdc
    FOR EACH ROW
    EXECUTE FUNCTION atd16_patrimoine_foncier.f_date_maj();
