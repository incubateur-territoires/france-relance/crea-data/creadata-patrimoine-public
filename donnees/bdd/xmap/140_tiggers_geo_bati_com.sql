-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/07/02 : DC / Création de la fonction et du trigger de suppression des panneaux d information local
-- 2022/04/08 : DC / Création dde la fonction qui permet de récupérer la clé de l'ign qui sert pour faire une jointure dans les vues
-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table geo_bati_com                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                 Remonter du nom du propriétaire
###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################

CREATE FUNCTION atd16_patrimoine_foncier.f_proprietaire()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN 
    
    select parc.nom 
	from pci.v_geo_parcelle_encours as parc 
	where st_intersects(ST_SetSRID(NEW.the_geom,0), parc.the_geom)
	-- Le ST_SetSRID à 0 est nécessaire dans Postgis 3. En effet les nouveaux objets arrivent en 4326.
	into  NEW.nom_prop;
    return NEW;

END;
$BODY$;

ALTER FUNCTION atd16_patrimoine_foncier.f_proprietaire()
    OWNER TO sditecgrp;

-- #################################################################### Trigger #####################################################################
	CREATE TRIGGER t_b_iu_propritaire_bati_com
    BEFORE INSERT OR UPDATE 
    ON atd16_patrimoine_foncier.geo_bati_com
    FOR EACH ROW
    EXECUTE PROCEDURE atd16_patrimoine_foncier.f_proprietaire();
	
-- ##################################################################################################################################################
-- ###                                 Suppression des contrôles suite à la suppression du bâtiment                      ###
-- ##################################################################################################################################################

-- #################################################################### Fonction ####################################################################
CREATE OR REPLACE FUNCTION  atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_com()

RETURNS trigger AS

$BODY$ 

BEGIN
DELETE FROM atd16_patrimoine_foncier.ngeo_controle_bati_com
WHERE (id_gid) = (OLD.gid) ;
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_com()
OWNER TO sditecgrp;
GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_com() TO public;
GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_com() TO sditecgrp;


-- #################################################################### Trigger #####################################################################
CREATE TRIGGER d_a_controle_bati_com
AFTER DELETE
ON atd16_patrimoine_foncier.geo_bati_com
FOR EACH ROW
EXECUTE FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_com();


    -- ##################################################################################################################################################
-- ###                                 Suppression des travaux suite à la suppression du bâtiment                   ###
-- ##################################################################################################################################################

-- #################################################################### Fonction ####################################################################
CREATE FUNCTION atd16_patrimoine_foncier.f_delete_travaux_com()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
 DELETE FROM atd16_patrimoine_foncier.ngeo_bati_travaux_com AS a
 WHERE (a.id_gid) =(OLD.gid);
RETURN NEW;
END;
$BODY$;

ALTER FUNCTION atd16_patrimoine_foncier.f_delete_travaux_com()
    OWNER TO sditecgrp;

-- #################################################################### Trigger #####################################################################
CREATE TRIGGER t_a_delete_delete_travaux_com
    AFTER DELETE
    ON atd16_patrimoine_foncier.geo_bati_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_patrimoine_foncier.f_delete_travaux_com();

    -- ##################################################################################################################################################
-- ###                                 Insertion de la clé de l'IGN                   ###
-- ##################################################################################################################################################

CREATE OR REPLACE FUNCTION atd16_patrimoine_foncier.f_cleabs_ign()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN 
    
    select  bat.cleabs 
	from ign_bdtopo.geo_batiment as bat 
	where st_intersects(ST_SetSRID(NEW.the_geom,0), bat.the_geom)  into  NEW.cleabs;
    return NEW;

END;
$BODY$;

ALTER FUNCTION atd16_patrimoine_foncier.f_cleabs_ign()
    OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_cleabs_ign() TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_cleabs_ign() TO PUBLIC;
