-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/27 : DC / Création de la fonction et du trigger 

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table geo_parcelle_com                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                 Remonter du nom du propriétaire, numéro de parcelle et surface
###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################


CREATE FUNCTION atd16_patrimoine_foncier.f_prop_refcad_surf()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN 
    
    select  parc.nom, parc.ident, parc.supf 
	from pci.geo_parcelle as parc 
	where st_intersects(ST_SetSRID(NEW.the_geom,0), parc.the_geom)  into  NEW.nom_proprietaire, NEW.ident, NEW.surface;
    return NEW;

END;
$BODY$;

ALTER FUNCTION atd16_patrimoine_foncier.f_prop_refcad_surf()
    OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_prop_refcad_surf() TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_prop_refcad_surf() TO PUBLIC;



-- #################################################################### Trigger #####################################################################

CREATE TRIGGER b_i_u_geo_parcelle
    BEFORE INSERT OR UPDATE 
    ON atd16_patrimoine_foncier.geo_parcelle_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_patrimoine_foncier.f_prop_refcad_surf();
	
