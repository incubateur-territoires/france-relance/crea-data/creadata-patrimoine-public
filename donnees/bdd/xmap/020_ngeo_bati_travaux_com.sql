-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git
-- 2022/04/07 : DC / Mise à jour du type de champ de descriptif travaux

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table non geographique : Des travaux des bâtiments des communes                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


CREATE TABLE atd16_patrimoine_foncier.ngeo_bati_travaux_com
(
  gid serial NOT NULL,--[PK][ATD16] Identifiant unique généré automatiquement
  id_gid integer,--[ATD16] Identifiant du bâtiment
  ident character varying (255),/*reference*/
  descriptif_travaux integer,--[ATD16] Liste deroulante lst_travaux
  autre_travaux character varying(255),--[ATD16] Si autre travaux que ceux mentionné dans la liste
  date_travaux date,--date des travaux
  nom_entreprise character varying(255),--[ATD16]Nom de l'entreprise des travaux
  montant character varying(10),--[ATD16]prix des travaux
  observations character varying(255),--[ATD16] Diverses observations
  date_creation timestamp,--[ATD16]Date de création de la donnée 
  date_maj timestamp,--[ATD16] Date de mise à jour de la donnée
  
  CONSTRAINT pk_ngeo_bati_travaux_com PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE atd16_patrimoine_foncier.ngeo_bati_travaux_com
  OWNER TO sditecgrp;


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_i_date_creation_bati_travaux_com
BEFORE INSERT
ON atd16_patrimoine_foncier.ngeo_bati_travaux_com
FOR EACH ROW
EXECUTE PROCEDURE atd16_patrimoine_foncier.f_datecreation();

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_u_date_maj_ngeo_bati_travaux_com
    BEFORE UPDATE 
    ON atd16_patrimoine_foncier.ngeo_bati_travaux_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_patrimoine_foncier.f_date_maj();
	
