-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/09/09 : DC / Intégration de la table
-- 2022/04/08 : DC / Création de la valeur Locatif (fermage agricole)

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur des types de parcelle                                                 ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################ls
CREATE TABLE atd16_patrimoine_foncier.lst_categorie_bati
( 
  gid serial NOT NULL, 
  libelle character varying(255) ,
  code integer,
  "order" integer,
  CONSTRAINT pk_lst_parcelle_type PRIMARY KEY (gid)
)WITH (
  OIDS=FALSE
);
ALTER TABLE atd16_gestion_territoriale.lst_parcelle_type
  OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_gestion_territoriale.lst_parcelle_type   TO sditecgrp;

/* Liste de valeur des types de parcelle*/

INSERT INTO atd16_gestion_territoriale.lst_parcelle_type   (code, libelle, "order")
	VALUES

('01','Voirie','1'),
('02','Agricole','2'),
('03','En friche','3'),
('04','Friche industrielle','4'),
('05','Bois','5'),
('06','Autre','6'),
('07','Inconnu','7')
('08','Locatif (fermage agricole)','8');
