-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur de la fonction du bâtiment                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_patrimoine_foncier.lst_fonction_batiment
(
    gid serial NOT NULL,
    code character varying(10),
    libelle character varying(256),
	"order" integer ,
    CONSTRAINT pk_lst_fonction_batiment PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
;
ALTER TABLE atd16_patrimoine_foncier.lst_fonction_batiment
    OWNER to sditecgrp;

GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_fonction_batiment TO sditecgrp;

/* Liste de valeur des matiere de la toiture*/

INSERT INTO atd16_patrimoine_foncier.lst_fonction_batiment (code, libelle, "order")
	VALUES
('01','Commerce','1'),
('02','Habitation','2'),
('03','Association','3'),
('04','Admninistratif','4'),
('05','Technique','5'),
('06','Autre','6');
