-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur des types de contrôle                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


CREATE TABLE atd16_patrimoine_foncier.lst_objet_controle (
  gid serial NOT NULL, 
  libelle character varying(255) ,
  code integer,
  "order" integer,
  CONSTRAINT pk_lst_objet_controle PRIMARY KEY (gid)
)WITH (
  OIDS=FALSE
);
ALTER TABLE atd16_patrimoine_foncier.lst_objet_controle
  OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_objet_controle   TO sditecgrp;

/* Liste de valeur des types objet de controle*/

INSERT INTO atd16_patrimoine_foncier.lst_objet_controle   (code, libelle, "order")
	VALUES
('00','','0'),
('01','Matériel incendie','1'),
('02','Détecteur de fumée','2'),
('03','Électricité','3'),
('04','Gaz','4'),
('05','Chauffage','5'),
('06','Désenfumage','6'),
('07','Grande cuisine +20kw','7'),
('08','Ascenseur','8');
