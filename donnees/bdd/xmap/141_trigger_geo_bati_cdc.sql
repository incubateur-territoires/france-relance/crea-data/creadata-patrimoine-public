-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Création de la fonction et du trigger des noms des propriétaire

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table ngeo_bati_cdc                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                 Remonter du noms des propriétaires                     ###
-- ##################################################################################################################################################

-- #################################################################### Fonction ####################################################################
La fonction est la même que celle utilisée pour la couche geo_bati_com

-- #################################################################### Trigger #####################################################################

CREATE TRIGGER t_b_iu_propritaire_bati_cdc
BEFORE INSERT OR UPDATE 
ON atd16_patrimoine_foncier.geo_bati_cdc
FOR EACH ROW
EXECUTE PROCEDURE atd16_patrimoine_foncier.f_proprietaire();
	
-- ##################################################################################################################################################
-- ###                                 Suppression des contrôles suite à la suppression du bâtiment                      ###
-- ##################################################################################################################################################

-- #################################################################### Fonction ####################################################################
CREATE OR REPLACE FUNCTION  atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_cdc()

RETURNS trigger AS

$BODY$ 

BEGIN
DELETE FROM atd16_patrimoine_foncier.ngeo_controle_bati_cdc
WHERE (id_gid) = (OLD.gid) ;
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_cdc()
OWNER TO sditecgrp;
GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_cdc() TO public;
GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_cdc() TO sditecgrp;

-- #################################################################### Trigger #####################################################################
CREATE TRIGGER d_a_controle_bati_cdc
AFTER DELETE
ON atd16_patrimoine_foncier.geo_bati_cdc
FOR EACH ROW
EXECUTE FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_bati_cdc();


    ##################################################################################################################################################
-- ###                                 Suppression des travaux suite à la suppression du bâtiment                   ###
-- ##################################################################################################################################################

-- #################################################################### Fonction ####################################################################
CREATE OR REPLACE FUNCTION  atd16_patrimoine_foncier.f_delete_ngeo_travaux_bati_cdc()

RETURNS trigger AS

$BODY$ 

BEGIN
DELETE FROM atd16_patrimoine_foncier.ngeo_bati_travaux_cdc
WHERE (id_gid) = (OLD.gid) ;
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_travaux_bati_cdc()
OWNER TO sditecgrp;
GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_travaux_bati_cdc() TO public;
GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_travaux_bati_cdc() TO sditecgrp;

-- #################################################################### Trigger #####################################################################
CREATE TRIGGER d_a_controle_trvaux_cdc
AFTER DELETE
ON atd16_patrimoine_foncier.geo_bati_cdc
FOR EACH ROW
EXECUTE FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_travaux_bati_cdc();
