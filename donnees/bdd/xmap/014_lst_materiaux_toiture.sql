-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur des matière de toiture                                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


CREATE TABLE atd16_patrimoine_foncier.lst_materiaux_toiture
(
    gid serial NOT NULL,
    code character varying(10),
    libelle character varying(256),
	"order" integer ,
    CONSTRAINT pk_lst_materiaux_toiture PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
;
ALTER TABLE atd16_patrimoine_foncier.lst_materiaux_toiture
    OWNER to sditecgrp;

GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_materiaux_toiture TO sditecgrp;

/* Liste de valeur des matiere de la toiture*/

INSERT INTO atd16_patrimoine_foncier.lst_materiaux_toiture  (code, libelle, "order")
	VALUES
('00','Autre','10'),
('01','Tuile','1'),
('02','Ardoise','2'),
('03','Végétal','3'),
('04', 'Bois','4'),
('05','Tôle','5'),
('06','Zinc','6'),
('07','Pierre','7'),
('08','Photovoltaïque','8');
