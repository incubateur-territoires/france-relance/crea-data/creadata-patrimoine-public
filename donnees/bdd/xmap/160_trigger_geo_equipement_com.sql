-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/04/21 : DC / Création de la fonction et du trigger pour récuperer les numéro de parcelle
-- 2022/04/22 : DC / Création de la fonction et du trigger pour supprimer les contrôles suite  à la supression de l'équipement
-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                      Fonctions triggers et triggers spécifiques à la table geo_parcelle_com                     			  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                				 Remonter du nom du numéro de parcelle et code insee														  ###
-- ##################################################################################################################################################


-- #################################################################### Fonction ####################################################################



CREATE FUNCTION atd16_patrimoine_foncier.f_refcad_insee()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN 
    
    select  parc.ident, parc.insee
	from pci.geo_parcelle as parc 
	where st_intersects(ST_SetSRID(NEW.the_geom,0), parc.the_geom)  into   NEW.parcelle, NEW.insee;
    return NEW;

END;
$BODY$;

ALTER FUNCTION atd16_patrimoine_foncier.f_refcad_insee()
    OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_refcad_insee() TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_refcad_insee() TO PUBLIC;



-- #################################################################### Trigger #####################################################################

CREATE TRIGGER b_i_u_geo_parcelle
    BEFORE INSERT OR UPDATE 
    ON atd16_patrimoine_foncier.geo_equipement_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_patrimoine_foncier.f_refcad_insee();

-- ##################################################################################################################################################
-- ###                                 Suppression des contrôles suite à la suppression de l'équipement                                        ###
-- ##################################################################################################################################################

-- #################################################################### Fonction ####################################################################
CREATE OR REPLACE FUNCTION  atd16_patrimoine_foncier.f_delete_ngeo_controle_equipement_com()

RETURNS trigger AS

$BODY$ 

BEGIN
DELETE FROM atd16_patrimoine_foncier.ngeo_controle_equipement_com
WHERE (id_gid) = (OLD.gid) ;
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_equipement_com()
OWNER TO sditecgrp;
GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_equipement_com() TO public;
GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_equipement_com() TO sditecgrp;


-- #################################################################### Trigger #####################################################################
CREATE TRIGGER d_a_controle_equipement_com
AFTER DELETE
ON atd16_patrimoine_foncier.geo_equipement_com
FOR EACH ROW
EXECUTE FUNCTION atd16_patrimoine_foncier.f_delete_ngeo_controle_equipement_com();

