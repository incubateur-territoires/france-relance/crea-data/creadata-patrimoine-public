-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/04/20 : DC / Création de la table
-- 2022/04/20 : DC / Création des index
-- 2022/04/21 : DC / Création des triggers générique date maj date création
-- 2022/04/21 : DC / Création de la vue des derniers contrôles
-- 2022/04/21 : DC / Création de la vue des prochains contrôles
-- 2022/04/21 : DC / Création de la vue des alertes des prochains contrôles
-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table géographique : des équipements des communes                                                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################



CREATE TABLE atd16_patrimoine_foncier.geo_equipement_com
(
    gid serial NOT NULL,--[PK][ATD16] Identifiant unique généré automatiquement
    insee character varying(6),--[ATD16] Code insee de la commune
    ident character varying(500),--[ATD16] Nom de l'équipement
	conforme boolean,--[ATD16] Équiement conforme aux exigenace de sécurité
	fabricant character varying(250),--[ATD16] Fabriquant de l'équipement
	modele character varying(250),--[ATD16] Modèle de l'équipement
	avertissement character varying,--[ATD16] Les avertissements nécessaires à la prévention des risques inhérents à l'utilisation de l'équipement.
	etat_equipement character varying (2),--[ATD16] Liste déroulante lst_etat, permet de notifier l'état de l'équipement
	date_etat Date, --[ATD16] Date à laquelle l'état de l'équipement est notifié
	parcelle character varying (15),--[ATD16] Numéro de la parcelle sur lequelle ce trouve l'équipement
    observation character varying(500),--[ATD16] Diverses observations
    date_creation timestamp,--[ATD16]Date de création de la donnée 
    date_maj timestamp,--[ATD16] Date de mise à jour de la donnée
    origdata character varying (254),--[ATD16] Origine de la donnée
    the_geom geometry,--[ATD16] Géometry du point
    CONSTRAINT pk_geo_equipement_com PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE atd16_patrimoine_foncier.geo_equipement_com
    OWNER to sditecgrp;

-- ################################################################## Commentaires ##################################################################
	
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.insee IS '[ATD16] Code insee de la commune';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.ident IS '[ATD16] Nom de l''équipement';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.conforme IS '[ATD16] Équiepment conforme aux exigenace de sécurité';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.fabricant IS '[ATD16] Fabriquant de l''équipement';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.modele IS '[ATD16] Modèle de l''équipement';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.avertissement IS '[ATD16] Les avertissements nécessaires à la prévention des risques inhérents à l''utilisation de l''équipement.';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.etat_equipement IS '[ATD16] Liste déroulante lst_etat, permet de notifier l''état de l''équipement';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.date_etat IS '[ATD16] Date à laquelle l''état de l''équipement est notifié';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.parcelle IS '[ATD16] Numéro de la parcelle sur lequelle ce trouve le bâtiment';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.observation IS '[ATD16]Diverses observations';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.date_creation IS '[ATD16] Date de création de la donnée';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.date_maj IS '[ATD16] Date de mise à jour de la donnée';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.observation IS '[ATD16]origdata';
COMMENT ON COLUMN atd16_patrimoine_foncier.geo_equipement_com.the_geom IS '[ATD16] Géometry du point';

-- ###################################################################### INDEX #####################################################################

-- DROP INDEX IF EXISTS atd16_patrimoine_foncier.geo_equipement_com_insee;

CREATE INDEX idx_geo_equipement_com_insee
  ON atd16_patrimoine_foncier.geo_equipement_com USING btree
  (insee COLLATE pg_catalog."default");

-- DROP INDEX IF EXISTS atd16_patrimoine_foncier.geo_equipement_com_the_geom_gist;

CREATE INDEX idx_geo_equipement_com_gist
  ON atd16_patrimoine_foncier.geo_equipement_com USING gist
  (the_geom);

-- DROP INDEX IF EXISTS atd16_patrimoine_foncier.geo_equipement_com_ident;

CREATE INDEX idx_geo_equipement_com_ident
  ON atd16_patrimoine_foncier.geo_equipement_com USING btree
  (ident COLLATE pg_catalog."default");


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

--    ##################################################################################################################################################
-- ###   v_date_dernier_controle_equipement_com : vue permettant de calculer la date du dernier contrôle de chaque bâtiment                       ###
-- ##################################################################################################################################################

--Pour que cette vue fonctionne parfaitement si l'objet du contrôle est le même exemple controle du but, objet du contrôle doit avoir exactement
-- le même non, sinon la vue sortira deux lignes différentes
CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_date_dernier_controle_equipement_com
 AS
Select


a.gid,
a.ident,
b.id_gid,
b.objet_du_controle,
MAX(b.date_du_controle) AS dernier_controle
			
FROM atd16_patrimoine_foncier.ngeo_controle_equipement_com b

LEFT JOIN atd16_patrimoine_foncier.geo_equipement_com as a  ON a.gid::text = b.id_gid::text 

GROUP BY (a.the_geom, b.id_gid, a.gid,a.ident, b.objet_du_controle);

-- ##################################################################################################################################################
-- ###   v_date_prochain_controle_equipement_com : vue permettant de faire remonter la date du prochain controle de chaque équipement             ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_date_prochain_controle_equipement_com
AS
Select


a.gid,
a.ident,
b.id_gid,
b.objet_du_controle,
MAX(b.date_du_prochain_controle) AS prochain_controle
			
FROM atd16_patrimoine_foncier.ngeo_controle_equipement_com b

LEFT JOIN atd16_patrimoine_foncier.geo_equipement_com as a  ON a.gid::text = b.id_gid::text 

GROUP BY (a.the_geom, b.id_gid, a.gid,a.ident, b.objet_du_controle);

-- ##################################################################################################################################################
-- ###  				                  				  Vue des prochains contrôles des équipements des communes                    							         ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_prochain_controle_equipement_com
AS
Select

a.gid,
b.insee,
b.the_geom,
b.ident,
d.dernier_controle,
a.prochain_controle,
a.objet_du_controle,
(((a.prochain_controle)- interval '3' MONTH) <= current_date) AS equipement_3_mois,
(((a.prochain_controle) - interval '1' MONTH) <= current_date)	AS equipement_1_mois,
(((a.prochain_controle) <= current_date)	OR  (c.date_du_prochain_controle IS NULL)) AS equipement_depasse

FROM atd16_patrimoine_foncier.v_date_prochain_controle_equipement_com AS a
LEFT JOIN atd16_patrimoine_foncier.geo_equipement_com AS b ON b.gid = a.gid
LEFT JOIN atd16_patrimoine_foncier.ngeo_controle_equipement_com AS c ON c.id_gid = b.gid
LEFT JOIN atd16_patrimoine_foncier.v_date_dernier_controle_equipement_com AS d ON d.gid = b.gid

GROUP BY (a.gid, b.insee, b.the_geom, b.ident, a.objet_du_controle,
a.prochain_controle, d.dernier_controle,
equipement_3_mois, equipement_1_mois,equipement_depasse);

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_i_date_creation_geo_equipement_com
BEFORE INSERT
ON atd16_patrimoine_foncier.geo_equipement_com
FOR EACH ROW
EXECUTE PROCEDURE atd16_patrimoine_foncier.f_datecreation();


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_u_date_maj_geo_equipement_com
    BEFORE UPDATE 
    ON atd16_patrimoine_foncier.geo_equipement_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_patrimoine_foncier.f_date_maj();
