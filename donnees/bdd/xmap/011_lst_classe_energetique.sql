-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur dde la classe energetique des bâtiments                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_patrimoine_foncier.lst_classe_energetique
(
    gid serial NOT NULL,
    code character varying(10),
    libelle character varying(256),
	"order" integer ,
    CONSTRAINT pk_lst_classe_energetique PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
;
ALTER TABLE atd16_patrimoine_foncier.lst_classe_energetique
    OWNER to sditecgrp;

GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_classe_energetique TO sditecgrp;

/* Liste de valeur de la classe énergetique*/

INSERT INTO atd16_patrimoine_foncier.lst_classe_energetique (code, libelle, "order")
	VALUES

('02','A','1'),
('03','B','2'),
('04','C','3'),
('05','D','4'),
('06','E','5'),
('07','F','6'),
('08','G','7'),
('09','Inconnu','8');
