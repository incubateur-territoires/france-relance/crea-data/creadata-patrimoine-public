-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git
-- 2022/04/07 : DC / Mise à jour du type de champ de type_de_controle et objet_du_controle

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table non géographique : du contrôle des batiments des communes                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


CREATE TABLE atd16_patrimoine_foncier.ngeo_controle_bati_com
(
    gid serial NOT NULL,--[PK][ATD16] Identifiant unique généré automatiquement
	id_gid integer,--[ATD16] Identifiant du bâtiment
    ident character varying(255) ,--[ATD16] Référence du contrôle
    date_du_controle date,--[ATD16] Date du contrôle
    objet_du_controle integer,--[ATD16]Liste deroulante lst_objet_controle
    type_de_controle integer,--[ATD16]Liste deroulante lst_technicien
    date_du_prochain_controle date,--[ATD16] Date du prochain contrôle
    date_creation timestamp without time zone,--[ATD16]Date de création de la donnée 
    date_maj timestamp without time zone,--[ATD16] Date de mise à jour de la donnée
    CONSTRAINT pk_ngeo_controle_bati_com PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE atd16_patrimoine_foncier.ngeo_controle_bati_com
    OWNER to sditecgrp;


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_i_date_creation_controle_bati_com
BEFORE INSERT
ON atd16_patrimoine_foncier.ngeo_controle_bati_com
FOR EACH ROW
EXECUTE PROCEDURE atd16_patrimoine_foncier.f_datecreation();


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_u_date_maj_ngeo_controle_bati_com
    BEFORE UPDATE 
    ON atd16_patrimoine_foncier.ngeo_controle_bati_com
    FOR EACH ROW
    EXECUTE FUNCTION atd16_patrimoine_foncier.f_date_maj();
