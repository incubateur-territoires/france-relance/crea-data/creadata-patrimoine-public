-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/27 : DC / Création de la table, des vue et des triggers

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table géographique : des parcelles des cdc                                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_patrimoine_foncier.geo_parcelle_cdc
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    insee character varying(6) ,--[ATD16] Code insee de la commune(Automatique)
    ident character varying(200),--[ATD16] Numéro parcelle (Automatique)
    surface double precision,--[ATD16] Surface (Automatique)
    date_acquisition date,--[ATD16] date d'qcuiqition de la parcelle
    presence_batiment boolean,--[ATD16] présence d'un bâtiment case à cocher
    type_parcelle character varying(2),--[ATD16]Liste déroulante lst_parcelle_type
    observation character varying(254),--[ATD16] diverse observation
    nom_proprietaire character varying(254),--[ATD16] Nom du propriétaire (Automatique)
    date_creation timestamp without time zone,--[ATD16]Date de création de la donnée (Automatique)
    date_maj timestamp,--[ATD16] Date de mise à jour de la donnée(Automatique)
    origdata character varying(254),--[ATD16] Origine de la donnée
    the_geom geometry,--[ATD16] Géometry du point
    CONSTRAINT pk_geo_parcelle_cdc PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE atd16_patrimoine_foncier.geo_parcelle_cdc
    OWNER to sditec;

GRANT ALL ON TABLE atd16_patrimoine_foncier.geo_parcelle_cdc TO sditec;

GRANT ALL ON TABLE atd16_patrimoine_foncier.geo_parcelle_cdc TO sditecgrp;

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###   v_geo_parcelle_cdc : vue permettant une visibilité  des parcelles des cdc                    ###
-- ##################################################################################################################################################

CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_geo_parcelle_cdc
 AS
 SELECT 
	a.gid,
	a.insee,
	a.ident,
    a.surface,
	a.date_acquisition,
	a.presence_batiment,
	a.type_parcelle,
	a.observation,
	a.nom_proprietaire,
	b.the_geom
	
   FROM atd16_patrimoine_foncier.geo_parcelle_cdc as a  
  LEFT JOIN pci.geo_parcelle as b  ON a.ident = b.ident;


  
  ALTER TABLE atd16_patrimoine_foncier.v_geo_parcelle_cdc
    OWNER TO sditecgrp;

  
-- ##################################################################################################################################################
-- ###   v_geo_parcelle_cdc_bati : vue permettant une visibilité  des parcelles des cdc avec un bâtiment                    ###
-- ##################################################################################################################################################


    CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_geo_parcelle_cdc_bati
 AS
 SELECT 
	a.gid,
	a.insee,
	a.ident,
    a.surface,
	a.date_acquisition,
	a.presence_batiment,
	a.type_parcelle,
	a.observation,
	a.nom_proprietaire,
	b.the_geom
	
   FROM atd16_patrimoine_foncier.geo_parcelle_cdc as a  
  LEFT JOIN pci.geo_parcelle as b  ON a.ident = b.ident
  
  Where a.presence_batiment ='true';
  
  
  ALTER TABLE atd16_patrimoine_foncier.v_geo_parcelle_cdc_bati
    OWNER TO sditecgrp;	

   -- ##################################################################################################################################################
-- ###   v_geo_parcelle_cdc_ss_batiment : vue permettant une visibilité  des parcelles des cdc sans un bâtiment                    ###
-- ##################################################################################################################################################
 

 CREATE OR REPLACE VIEW atd16_patrimoine_foncier.v_geo_parcelle_cdc_ss_batiment
 AS
 SELECT 
	a.gid,
	a.insee,
	a.ident,
    a.surface,
	a.date_acquisition,
	a.presence_batiment,
	a.type_parcelle,
	a.observation,
	a.nom_proprietaire,
	b.the_geom
	
   FROM atd16_patrimoine_foncier.geo_parcelle_cdc as a  
  LEFT JOIN pci.geo_parcelle as b  ON a.ident = b.ident
  
  Where a.presence_batiment IS NULL;
  
  
  ALTER TABLE atd16_patrimoine_foncier.v_geo_parcelle_cdc_ss_batiment
    OWNER TO sditecgrp;	
		

		
-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                          Trigger(s) générique(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################


CREATE TRIGGER t_before_i_date_creation_geo_parcelle_cdc
BEFORE INSERT
ON atd16_patrimoine_foncier.geo_parcelle_cdc
FOR EACH ROW
EXECUTE PROCEDURE atd16_patrimoine_foncier.f_datecreation();

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

CREATE TRIGGER t_before_u_date_maj_geo_parcelle_cdc
    BEFORE UPDATE 
    ON atd16_patrimoine_foncier.geo_parcelle_cdc
    FOR EACH ROW
    EXECUTE FUNCTION atd16_patrimoine_foncier.f_date_maj();
