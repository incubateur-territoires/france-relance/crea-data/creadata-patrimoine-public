-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/27 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur des types de parcelles                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


CREATE TABLE atd16_patrimoine_foncier.lst_parcelle_type
(
    gid serial NOT NULL,
    libelle character varying(255),
    code integer,
    "order" integer,
    CONSTRAINT pk_lst_parcelle_type PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE atd16_patrimoine_foncier.lst_parcelle_type
    OWNER to sditecgrp;
    
/* Liste de valeur des types de categorie*/	
	
INSERT INTO atd16_patrimoine_foncier.lst_parcelle_type   (code, libelle, "order")
	VALUES

('01','Voirie','1'),
('02','Agricole','2'),
('03','En friche','3'),
('04','Friche industrielle','4'),
('05','Bois','5'),
('06','Autre','6'),
('07','Inconnu','7');
