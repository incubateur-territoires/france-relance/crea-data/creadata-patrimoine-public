-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur de la liste des travaux                                         ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_patrimoine_foncier.lst_travaux
(
    gid serial NOT NULL,
    code integer,
    libelle character varying(256),
	"order" integer ,
    CONSTRAINT pk_lst_travaux PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
;
ALTER TABLE atd16_patrimoine_foncier.lst_travaux
    OWNER to sditecgrp;

GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_travaux TO sditecgrp;

/* Liste de valeur des travaux*/

INSERT INTO atd16_patrimoine_foncier.lst_travaux(code, libelle, "order")
	VALUES
('00','','00'),
('01','Réfection toiture','1'),
('02','Démoussage','2'),
('03','Nettoyage gouttière','3'),
('04', 'Rénovation menuiserie','4'),
('05','Plomberie','5'),
('06','Remplacements des sols','6'),
('07','Électricité','7'),
('08','Peinture','8'),
('09','Isolation exterieur','9'),
('10','Remise à neuf complet','10'),
('11','Rénovation parking','11'),
('99','Autre','99');
