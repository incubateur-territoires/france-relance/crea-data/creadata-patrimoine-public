-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/04/20 : DC / Création de la table

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur de la classe des états des équipements                              ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_patrimoine_foncier.lst_etat_equipement
(
    gid serial NOT NULL,
    code character varying(10),
    libelle character varying(256),
	"order" integer ,
    CONSTRAINT pk_lst_etat_equipement PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
;
ALTER TABLE atd16_patrimoine_foncier.lst_etat_equipement
    OWNER to sditecgrp;

GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_etat_equipement TO sditecgrp;

/* Liste de valeur de la classe énergetique*/

INSERT INTO atd16_patrimoine_foncier.lst_etat_equipement (code, libelle, "order")
	VALUES


('01','NC','2'),
('02','Bon','3'),
('03','Moyen','4'),
('04','Mauvais','5');

-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE atd16_patrimoine_foncier.lst_etat_equipement IS '[ARC] Domaine de valeur des codes du type de site intégrant les objets des espaces verts';

COMMENT ON COLUMN atd16_patrimoine_foncier.lst_etat_equipement.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN atd16_patrimoine_foncier.lst_etat_equipement.code IS '[ARC] Code de l''état de l''équipement';
COMMENT ON COLUMN atd16_patrimoine_foncier.lst_etat_equipement.libelle IS '[ARC] Libellé de l''état de l''équipement';
COMMENT ON COLUMN atd16_patrimoine_foncier.lst_etat_equipement.order IS '[ARC] Ordre des valeurs dans la liste';
