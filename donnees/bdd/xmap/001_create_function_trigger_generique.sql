-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : SL / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

CREATE FUNCTION atd16_patrimoine_foncier.f_datecreation()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.date_creation = now();
    RETURN NEW;   
END;
$BODY$;

ALTER FUNCTION atd16_patrimoine_foncier.f_datecreation()
    OWNER TO sditecgrp;

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

CREATE FUNCTION atd16_patrimoine_foncier.f_date_maj()
RETURNS trigger
LANGUAGE 'plpgsql'
COST 100
VOLATILE NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.date_maj = now(); -- À la modification d'un objet, le champ date_maj prend la valeur de la date du jour
    RETURN NEW;
END;
$BODY$;

ALTER FUNCTION atd16_patrimoine_foncier.f_date_maj()
    OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_date_maj() TO sditecgrp;

GRANT EXECUTE ON FUNCTION atd16_patrimoine_foncier.f_date_maj() TO PUBLIC;

COMMENT ON FUNCTION atd16_patrimoine_foncier.f_date_maj()
    IS 'Fonction trigger permettant la mise à jour du champ date_maj';

