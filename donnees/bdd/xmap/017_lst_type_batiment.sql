-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur des types de bâtiment erpbâtiments                                          ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################




CREATE TABLE atd16_patrimoine_foncier.lst_type_batiment
(
    gid serial NOT NULL,
    code character varying(10),
    libelle character varying(256),
	"order" integer, 
    CONSTRAINT pk_lst_type_batiment PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
;
ALTER TABLE atd16_patrimoine_foncier.lst_type_batiment
    OWNER to sditecgrp;

GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_type_batiment TO sditecgrp;

/* Liste de valeur des types de batiments*/

INSERT INTO atd16_patrimoine_foncier.lst_type_batiment  (code, libelle,"order")
	VALUES
('00','J : Établissements de soins','00'),
('01','L : Salle d''audition, de conférence, de spectacles ou à usages multiples','01'),
('02','M : Magasins, centres commerciaux','02'),
('03','N : Restaurants, débits de boissons','03'),
('04','O : Hôtels et pensions de famille','04'),
('05','P : Salles de danse et de jeux','05'),
('06','R : Établissements d''enseignement, colonies de vacances','06'),
('07','S : Bibliothèques, centres de documentation','07'),
('08','T : Salles d''exposition','08'),
('09','U : Établissements sanitaires (hôpitaux,cliniques)','09'),
('10','V : Établissements de culte','10'),
('11','W : Administrations, banques et bureaux','11'),
('12','X : Établissements sportifs couverts','12'),
('13','Y : Musées','13'),
('14','PA : Établissements de plein air','14'),
('15','CTS : Chapiteaux, tentes et structures','15'),
('16','SG : Structures gonflables','16'),
('17','PS : Parcs de stationnement couverts','17'),
('18','GA : Gares accessibles au public','18'),
('19','PA : Établissements de plein air','19'),
('21','EF : établissements flottants','20');
