-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/08/24 : DC / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table liste : Domaine de valeur des categories de batiment                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE atd16_patrimoine_foncier.lst_categorie_bati (
  gid serial NOT NULL, 
  libelle character varying(255) ,
  code integer,
  "order" integer,
  CONSTRAINT pk_lst_categorie_bati PRIMARY KEY (gid)
)WITH (
  OIDS=FALSE
);
ALTER TABLE atd16_patrimoine_foncier.lst_categorie_bati
  OWNER TO sditecgrp;
GRANT ALL ON TABLE atd16_patrimoine_foncier.lst_categorie_bati   TO sditecgrp;

/* Liste de valeur des types de categorie*/

INSERT INTO atd16_patrimoine_foncier.lst_categorie_bati   (code, libelle, "order")
	VALUES

('01','1ère catégorie','1'),
('02','2ème catégorie','2'),
('03','3ème catégorie','3'),
('04','4ème catégorie','4'),
('05','5ème catégorie','5');
