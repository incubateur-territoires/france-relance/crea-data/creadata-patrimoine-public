# Patrimoine public

Le module du patrimoine public est fait pour permettre aux communes de recenser leurs bâtiments et parcelles en renseignent diverses informations. Ce qui permet d'avoir des informations centralisés et une visualisations cartographique.

# Principe de fonctionnement

**Bâtiment du patrimoine public**
* Des tables géographique permettent de géologaliser les bâtiments et de renseigner des informations commme la classe erp, la fonction du bâtiment, la classe énergetique et les matériaux.
* Le positionnement du point permet de faire remonter les informations existantes de l'ign dans une vue.
* Une table non geographique permmet à l'utilisateur de renseigner des contrôle du bâtiment comme le matériel à incendie, les détecteurs de fumée ect... Ce qui lui permettra d'avoir une alerte visuelle sur les contrôles qui seront à éffectués dans les 3 mois à venir.
* Une table non geographique permet d'historiser les travaux effectué sur le bâtiment avec la nature, la date l'entreprise ... des travaux.



#Choses à faire


* Notice d'utlisation à corriger par seb et romain
* Étudier la demande de gestion des équipements
* Réflechir à la demande de gestion des poteaux incendies 



# Ressources

* [Les bonnes pratiques dans GitLab](https://git.atd16.fr/cesig/gestion-de-git/-/blob/master/les_bonnes_pratiques_gitlab.md)
* [MCD du patrimoine foncier](ressources/Mcd_patrimoine_foncier_v1.pdf)
* [MLD du patrimoine foncier](ressources/Mld_patrimoine_foncier_v1.pdf)
* [Schéma applicatif des parcelles](ressources/Sch%C3%A9ma_applicatif_patrimoine_foncier_parcelle.pdf)
* [Schéma applicatif des bâtiments](ressources/Sch%C3%A9ma_applicatif_patrimoine_foncier_batiment.pdf)
