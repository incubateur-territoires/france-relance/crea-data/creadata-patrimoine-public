--################################################################# SUIVI CODE SQL #################################################################

-- 2022/04/22 : DC / Création du script

-- ##################################################################################################################################################
-- ###                                                                                                                                         ###
--###                                       Fiche pour x'map des batiments des communes et des cdc                                      	   ###
--###                                                                                                                                          ###
--################################################################################################################################################


<!--
Fiche exemple. A renommer comme suit : [SCHEMA]_[TABLE].php pour tester
-->

<?php
	//Affichage de la fiche standard
	//echo $this->getFicheStandard();
?>

<div id="sw_drawing_attForm" class="tabbable">
    <h3 class="smaller blue no-margin-top">
                Bâtiment communal
            </h3>
            <!--ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab-fiche-general" data-toggle="tab">Saisie d'une adresse</a>
                </li>
            </ul-->
   <form id="drawingFormPiv" class="form-horizontal">
      <?php
		//echo $this->getHiddenField($this->properties['gid']);
		$prop['insee'] = $this->layerProperties['insee'];

		if (is_array($prop['insee']['value']))
				{
				$colInsee = $prop['insee']['field']['intersects']['value'];
				$prop['insee']['value'] = $prop['insee']['value'][0][$colInsee];
				}

		?>
      <!-- Général -->	 
	  
				<ul class="nav nav-tabs" id="tabsFichePEVC">
				<li class="active"><a data-toggle="tab" href="#tabs-1">Général</a></li>
				</ul>
				<div class="tab-content">
                    <div class="tab-pane fade in active" id="tabs-1">
						<div class="form-group">
							<div class="col-sm-3">
								%%numero_batiment_label%%
							</div>
							<div class="col-sm-9">
								%%numero_batiment_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%ident_label%%
							</div>
							<div class="col-sm-9">
								%%ident_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%batiment_erp_label%%
							</div>
							<div class="col-sm-9">
								%%batiment_erp_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%categorie_label%%
							</div>
							<div class="col-sm-9">
								%%categorie_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%type_batiment_label%%
							</div>
							<div class="col-sm-9">
								%%type_batiment_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%sommeil_label%%
							</div>
							<div class="col-sm-9">
								%%sommeil_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%fonction_batiment_label%%
							</div>
							<div class="col-sm-9">
								%%fonction_batiment_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%utilisation_batiment_label%%
							</div>
							<div class="col-sm-9">
								%%utilisation_batiment_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%surface_label%%
							</div>
							<div class="col-sm-9">
								%%surface_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%hauteur_label%%
							</div>
							<div class="col-sm-9">
								%%hauteur_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%location_label%%
							</div>
							<div class="col-sm-9">
								%%location_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%materiaux_toiture_label%%
							</div>
							<div class="col-sm-9">
								%%materiaux_toiture_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%materiaux_mur_label%%
							</div>
							<div class="col-sm-9">
								%%materiaux_mur_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%autre_materiaux_mur_label%%
							</div>
							<div class="col-sm-9">
								%%autre_materiaux_mur_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%classe_energetique_label%%
							</div>
							<div class="col-sm-9">
								%%classe_energetique_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%observation_label%%
							</div>
							<div class="col-sm-9">
								%%observation_value%%
							</div>
						</div>
                    </div>
				</div>
   </form>
</div>

<script type="text/javascript">
	//page prête
	$(document).ready(function() {	
		
		console.log('document ready');
		
		//Après l'ouverture de la fiche
		//On peut grace à cette évènement modifier la fiche avec jquery
		//2 paramètres :
		//   - bModif : true si cette modification d'objet. false si c'est un ajout
		//   - panel : l'objet jspanel de la fiche
		$( "#sw_drawing_attPanel" ).one( "afterShowAttPanel", function(e, data) {
			if (data.bModif) {
				console.log('Modification d\'objet');
			} else {
				console.log('Ajout d\'un nouvelle objet');
			}
		});
		
	});
	
</script>
