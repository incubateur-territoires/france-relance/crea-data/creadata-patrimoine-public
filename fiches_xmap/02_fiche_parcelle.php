--################################################################# SUIVI CODE SQL #################################################################

-- 2022/04/22 : DC / Création du script

-- ##################################################################################################################################################
-- ###                                                                                                                                         ###
--###                                       Fiche pour x'map des parcelles des communes et des cdc                                      	   ###
--###                                                                                                                                          ###
--################################################################################################################################################



<!--
Fiche exemple. A renommer comme suit : [SCHEMA]_[TABLE].php pour tester
-->
<?php
	//Affichage de la fiche standard
	//echo $this->getFicheStandard();
?>
<div id="sw_drawing_attForm" class="tabbable">
    <h3 class="smaller blue no-margin-top">
                Parcelle communale
            </h3>
            <!--ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab-fiche-general" data-toggle="tab">Saisie d'une adresse</a>
                </li>
            </ul-->
   <form id="drawingFormPiv" class="form-horizontal">
      <?php
		//echo $this->getHiddenField($this->properties['gid']);
		$prop['insee'] = $this->layerProperties['insee'];

		if (is_array($prop['insee']['value']))
				{
				$colInsee = $prop['insee']['field']['intersects']['value'];
				$prop['insee']['value'] = $prop['insee']['value'][0][$colInsee];
				}
		?>
      <!-- Général -->	 
	  
				<ul class="nav nav-tabs" id="tabsFichePEVC">
				<li class="active"><a data-toggle="tab" href="#tabs-1">Général</a></li>
				</ul>
				<div class="tab-content">
                    <div class="tab-pane fade in active" id="tabs-1">
						<div class="form-group">
							<div class="col-sm-3">
								%%date_acquisition_label%%
							</div>
							<div class="col-sm-9">
								%%date_acquisition_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%presence_batiment_label%%
							</div>
							<div class="col-sm-9">
								%%presence_batiment_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%type_parcelle_label%%
							</div>
							<div class="col-sm-9">
								%%type_parcelle_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%observation_label%%
							</div>
							<div class="col-sm-9">
								%%observation_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%origdata_label%%
							</div>
							<div class="col-sm-9">
								%%origdata_value%%
							</div>
						</div>
                    </div>
				</div>
   </form>
</div>

<script type="text/javascript">
	//page prête
	$(document).ready(function() {	
		
		console.log('document ready');
		
		//Après l'ouverture de la fiche
		//On peut grace à cette évènement modifier la fiche avec jquery
		//2 paramètres :
		//   - bModif : true si cette modification d'objet. false si c'est un ajout
		//   - panel : l'objet jspanel de la fiche
		$( "#sw_drawing_attPanel" ).one( "afterShowAttPanel", function(e, data) {
			if (data.bModif) {
				console.log('Modification d\'objet');
			} else {
				console.log('Ajout d\'un nouvelle objet');
			}
		});
		
	});
	
</script>
